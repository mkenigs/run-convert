# COM_GITLAB_TOKEN needs to be set or changed
export GITLAB_TOKENS='{"gitlab.com": "COM_GITLAB_TOKEN"}'

ARM=284003324
BLOCK=292420324
BPF=310057327
KVM=311657149
NET_NEXT=313892242
RDMA=313935605
RHEL7=310831164
RHEL8=300641680
SCSI=300444010
UMB_RESULTS=313710527
UPSTREAM_STABLE=305761498

RHEL6=306937187
RHEL7=314244597
RHEL8=314032832

generate_diff() {
    project=$1
    pipeline=$2
    echo $pipeline
    IS_PRODUCTION=false python3 -m cki.kcidb.convert --output-stdout single pipeline gitlab.com $project ${!pipeline} | jq . > $pipeline-no-upt
    if [[ $(wc -l < $pipeline-no-upt) -le 6 ]]; then
        rm $pipeline-no-upt
        return
    fi
    echo $pipeline --upt
    IS_PRODUCTION=false python3 -m cki.kcidb.convert --output-stdout --upt single pipeline gitlab.com $project ${!pipeline} | jq . > ${pipeline}-upt
    if [[ $(wc -l < $pipeline-upt) -le 6 ]]; then
        rm $pipeline-upt
        return
    fi
    if diff ${pipeline}-no-upt ${pipeline}-upt > ${pipeline}-diff; then
        rm ${pipeline}-diff ${pipeline}-no-upt
        mv ${pipeline}-upt ${pipeline}
    fi
}

mkdir output
cd output


for pipeline in ARM BLOCK BPF KVM NET_NEXT RDMA RHEL7 RHEL8 SCSI UMB_RESULTS UPSTREAM_STABLE; do
    generate_diff redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/external-triggers $pipeline
done

for pipeline in RHEL6 RHEL7 RHEL8; do
    generate_diff redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/cki-internal-contributors $pipeline
done
